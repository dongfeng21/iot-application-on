"""book URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from app01 import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('pblist/',views.pblist),
    path('pbadd/',views.pbadd),
    path('pbdel/',views.pbdel),
    path('pbedit/',views.pbedit),
    path('bklist/',views.bklist),
    path('bkadd/',views.bkadd),
    path('bkdel/',views.bkdel),
    path('bkedit/',views.bkedit),
    path('aulist/',views.aulist),
    path('auadd/',views.auadd),
    path('audel/',views.audel),
    path('auedit/',views.auedit),
    path('mb/',views.mb),
    path('login/',views.login),
    path('index_begin/',views.index_begin),
    path('index_2/',views.index_2),
    path('index_3/',views.index_3),
    path('index_4/',views.index_4),
    path('index_5/',views.index_5),
    path('denglu/',views.denglu),
    path('userlist/',views.userlist),
    path('userdel/',views.userdel),
    path('useredit/',views.useredit),
    path('zhuce/',views.zhuce),
    path('myself/',views.myself),
    path('chakan/',views.chakan),
    path('geshou/',views.geshou),
    # path('search/',views.search),







]
