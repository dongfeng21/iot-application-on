from pdb import post_mortem
from unicodedata import name
from urllib import request
from django import shortcuts
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.contrib.auth import authenticate,login
from app01 import models
def pblist(request):
    pbs = models.Publisher.objects.all()
    # for i in pbs:
    #     print(i.id,i.name)
    return render(request,'pblist.html',{'xxx':pbs})
def pbadd(request):
    if request.method == 'POST':
        pb_name = request.POST.get('pb_name')
        if not pb_name:
            return render(request,'pbadd.html',{'error':'出版社信息不能为空'})
        if models.Publisher.objects.filter(name=pb_name):
            return render(request,'pbadd.html',{'error':'出版社信息已存在'})
        else:
            ret = models.Publisher.objects.create(name=pb_name)
            return redirect('/pblist/')
    return render(request,'pbadd.html')
    # if request.method == 'POST':
    #     pb_name = request.POST.get('pb_name')
    #     ret = models.Publisher.objects.create(name=pb_name)
    #     return redirect('/pblist/')
    # return render(request,'pbadd.html')
def pbdel(request):
    pk = request.GET.get('pk')
    models.Publisher.objects.filter(pk=pk).delete()
    return redirect('/pblist/')
def pbedit(request):
    pk = request.GET.get('pk')
    pb_obj = models.Publisher.objects.get(pk=pk)

    if request.method == 'GET':
       
        return render(request,'pbedit.html',{'pb_obj':pb_obj})
    else:
        pb_name = request.POST.get("pb_name")
        pb_obj.name = pb_name
        pb_obj.save()
        return redirect('/pblist/')
def bklist(request):
    bks = models.Book.objects.all()
    # for book in bks:
    #     print(book.id,book.name,book.publisher_id)
    # return HttpResponse('books')
    return render(request,'bklist.html',{'bks':bks})
def bkadd(request):
    error = ''
    if request.method == 'POST':
        bk_name = request.POST.get('bk_name')
        pb_id = request.POST.get('pb_pk')
        if not bk_name:
            error ='书籍信息不能为空'
        elif models.Book.objects.filter(name=bk_name):
            error = '书籍信息已存在'
        else:
            models.Book.objects.create(name=bk_name,publisher_id=pb_id)
            return redirect('/bklist/')
    pbs = models.Publisher.objects.all()
    return render(request,'bkadd.html',{'pbs':pbs,'error':error})
def bkdel(request):
    pk = request.GET.get('pk')
    models.Book.objects.filter(pk=pk).delete()
    return redirect('/bklist/')
def bkedit(request):
    pk = request.GET.get('pk')
    bk_obj = models.Book.objects.get(pk=pk)
    if request.method == 'POST':
        bk_name = request.POST.get("bk_name")
        bk_neirong = request.POST.get("bk_neirong")
        pb_pk = request.POST.get("pb_pk")
        bk_obj.name = bk_name
        bk_obj.neirong = bk_neirong
        bk_obj.publisher_id = pb_pk
        bk_obj.save()
        return redirect('/bklist/')
    
    pbs = models.Publisher.objects.all()
    return render(request,'bkedit.html',{'bk_obj':bk_obj,'pbs':pbs})

def myself(request):
    pk = request.GET.get('pk')
    # pk = request.GET.get('pk')
    # bk_obj = models.Book.objects.get(pk=pk)
    # if request.method == 'POST':
    #     bk_name = request.POST.get("bk_name")
    #     bk_neirong = request.POST.get("bk_neirong")
    #     pb_pk = request.POST.get("pb_pk")
    #     # bk_obj.name = bk_name
    #     bk_obj.neirong = bk_neirong
    #     # bk_obj.publisher_id = pb_pk
    #     # bk_obj.save()
    #     # return redirect('/bklist/')
    
    pbs = models.Publisher.objects.all()
    bks = models.Book.objects.all()
    # return render(request,'myself.html',{'user_obj':user_obj,'bks':bks})
    # bks = models.Book.objects.all()
    # # for book in bks:
    # #     print(book.id,book.name,book.publisher_id)
    # # return HttpResponse('books')
    return render(request,'myself.html',{'bks':bks})
    # # users = models.Users.objects.all()
    # # return render(request,'myself.html',{"users":users})
def aulist(request):
    aus = models.Author.objects.all()
    for au in aus:
        print(au.id,au.name)
        print(au.books)
        print(au.books.all())
        print('*'*40)
    return render(request,'aulist.html',{'aus':aus})
def auadd(request):
    if request.method == 'POST':
        au_name = request.POST.get('au_name')
        bk_pks = request.POST.getlist('bk_pks')
        if not au_name:
            bks = models.Book.objects.all()
            return render(request,'auadd.html',{'bks':bks,'error':'作者信息不能为空'})
        au_obj = models.Author.objects.create(name=au_name)
        au_obj.books.set(bk_pks)
        return redirect('/aulist/')
    bks = models.Book.objects.all()
    return render(request,'auadd.html',{'bks':bks})
def audel(request):
    pk = request.GET.get('pk')
    models.Author.objects.filter(pk=pk).delete()
    return redirect('/aulist/')
def auedit(request):
    pk = request.GET.get('pk')
    au_obj = models.Author.objects.get(pk=pk)
    if request.method =='POST':
        au_name = request.POST.get('au_name')
        bk_pks = request.POST.getlist('bk_pks')
        au_obj.name = au_name
        au_obj.save()
        au_obj.books.set(bk_pks)
        return redirect('/aulist/')
    bks = models.Book.objects.all()
    return render(request,'auedit.html',{'au_obj':au_obj,'bks':bks})
def mb(request):
    return render(request,'mb.html')
def denglu(request):
    if request.method == 'POST':
       
        username = request.POST.get("username")
        password = request.POST.get("password")
        # users = auth.authenticate(username=username, password=password)        
    # if users:    
    #     auth.denglu(request, users) # 这里做了登录
    #     return redirect('/myself/') # 跳转至首页
        pk = models.Users.objects.filter(username=username).first().pk
        request .session['pk'] = pk
        if models.Users.objects.filter(username=username,password=password):
            # auth.denglu(request,users)
           return redirect('/index_begin/')
        else :
            error = '用户名或密码错误'
            
            
    return render(request,'denglu.html')
def login(request):
    if request.method == 'POST':
       
        user = request.POST.get("username")
        pwd = request.POST.get("pwd")
        if models.User.objects.filter(username=user,password=pwd):
            return redirect('/mb/')
       
    return render(request,'login.html')
def index_begin(request):
     
     return render(request,'index_begin.html')

def chakan(request):
     pk = request.GET.get('pk')
     bk_obj = models.Book.objects.get(pk=pk)
    #  if request.method == 'POST':
    #     bk_name = request.POST.get("bk_name")
    #     bk_neirong = request.POST.get("bk_neirong")
    #     pb_pk = request.POST.get("pb_pk")

        # bk_obj.name = bk_name
        # bk_obj.neirong = bk_neirong
        # bk_obj.publisher_id = pb_pk
        # bk_obj.save()

    #     return redirect('/myself/')
    
    #  pbs = models.Publisher.objects.all()
     return render(request,'chakan.html',{'bk_obj':bk_obj})

def geshou(request):
    pk = request.GET.get('pk')
    au_obj = models.Author.objects.get(pk=pk)
    # if request.method =='POST':
    #     au_name = request.POST.get('au_name')
    #     bk_pks = request.POST.getlist('bk_pks')

    #     au_obj.name = au_name
    #     au_obj.save()
    #     au_obj.books.set(bk_pks)

    #     return redirect('/index_2/')
    bks = models.Book.objects.all()
    return render(request,'geshou.html',{'au_obj':au_obj,'bks':bks})

def index_2(request):
     aus = models.Author.objects.all()
     for au in aus:
        print(au.id,au.name)
        print(au.books)
        print(au.books.all())
        print('*'*40)
     return render(request,'index_2.html',{'aus':aus})
    
def index_3(request):
    return render(request,'index_3.html')
def index_4(request):
    pk = request.GET.get('pk')
    
    
    pbs = models.Publisher.objects.all()
    bks = models.Book.objects.all()
   
    return render(request,'index_4.html',{'bks':bks})
   
    # return render(request,'index_4.html')
def index_5(request):
    return render(request,'index_5.html')
def userlist(request):
    users = models.Users.objects.all()
    # for user in users:
    #     print(user.id,user.username)
    #     print(user.books)
    #     print(user.books.all())
    #     print('*'*40)
    return render(request,'userlist.html',{'users':users})
def userdel(request):
    pk = request.GET.get('pk')
    models.Users.objects.filter(pk=pk).delete()
    return redirect('/userlist/')
def useredit(request):
    pk = request.GET.get('pk')
    user_obj = models.Users.objects.get(pk=pk)
    if request.method =='POST':
        user_name = request.POST.get('user_name')
        bk_pks = request.POST.getlist('bk_pks')
        user_obj.name = user_name
        user_obj.save()
        user_obj.books.set(bk_pks)
        return redirect('/userlist/')
    bks = models.Book.objects.all()
    return render(request,'useredit.html',{'user_obj':user_obj,'bks':bks})
def zhuce(request):
    error = ''
    if request.method == 'POST':
        user_name = request.POST.get('user_name')
        password = request.POST.get('password')
        user_age = request.POST.get('user_age')
        user_gender = request.POST.get('user_gender')

        if not user_name:
            error ='昵称不能为空'
        elif models.Users.objects.filter(username=user_name):
            error = '昵称已存在'
        elif not password:
            error ='密码不能为空'
        elif not user_age:
            error ='年龄不能为空'
        elif not user_gender:
            error ='性别不能为空'
        else:
            models.Users.objects.create(username=user_name,password=password,age=user_age,gender=user_gender)
            return redirect('/denglu/')
    # pbs = models.Publisher.objects.all()
    return render(request,'zhuce.html')
# def search(request):
   
#      pk = request.POST.get('pk')
#      bk_obj = models.Book.objects.get(pk=pk)
#      musiclist=models.Book.objects.filter(name__icontains=pk).all()
#     #  pbs = models.Publisher.objects.all()
#      return render(request,'chakan.html',{'musiclist':musiclist})

                                               
# Create your views here.,


