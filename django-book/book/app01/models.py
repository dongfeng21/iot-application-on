
from django.db import models
class Publisher(models.Model):
    name = models.CharField(max_length=32)
class Book(models.Model):
    name = models.CharField(max_length=32)
    neirong = models.CharField(max_length=64,default="歌词:")
    publisher = models.ForeignKey(Publisher,on_delete=models.CASCADE)
class Author(models.Model):
    name = models.CharField(max_length=32)
    books = models.ManyToManyField(Book)
class User(models.Model):
    username = models.CharField(max_length=32)
    password = models.CharField(max_length=32)
class Users(models.Model):
    username = models.CharField(max_length=32)
    password = models.CharField(max_length=32)
    age = models.CharField(max_length=32)
    gender = models.CharField(max_length=32)
    books = models.ManyToManyField(Book)
# Create your models here.
