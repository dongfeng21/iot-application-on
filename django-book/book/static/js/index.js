iGeciku.onload_(function (iGeciku) {
    var $ = iGeciku.$;

    $("#tabSelf").rTabs();

    P.initMathod({
        params: [
            {elemId: '#page', total: hotSongTotal, pageIndex: '1', pageSize: '15'}
        ]
    });
});

function toPage(ele) {
    var hotSongListDiv = $("#hotSongListDiv");
    hotSongListDiv.mLoading({
        text: "",
        icon: "",
        html: false,
        content: "",
        mask: true
    });

    hotSongListDiv.mLoading("show");
    iGeciku.ajax.get('song/hot', {'page': $(ele).attr("data-pageindex")}, function (res) {
        $.scrollTo('#hotSongListDiv', 300);
        $("#hotSongList").html(res);
        hotSongListDiv.mLoading("hide");
    });
}