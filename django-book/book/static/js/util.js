(function (win) {
    var iGeciku = {};// 命名空间

    function onload(callback) {
        iGeciku.$ = win.jQuery;

        /**
         * code定义
         */
        iGeciku.code = {
            SUCC: 1,// 服务器返回成功
            FAIL: 0, //服务器操作条件不足
            NOTLOGIN: -100,     //  没有权限
            NOTAUTH: 401,     //  没有权限
            ERROR: 500        // 	网络错误
        };

        //=============
        var Type = (function () {
            var type = {};
            var typeArr = ['String', 'Object', 'Number', 'Array', 'Undefined', 'Function', 'Null', 'Symbol'];
            for (var i = 0; i < typeArr.length; i++) {
                (function (name) {
                    type['is' + name] = function (obj) {
                        return Object.prototype.toString.call(obj) == '[object ' + name + ']';
                    };
                })(typeArr[i]);
            }
            return type;
        })();
        iGeciku.Type = Type;

        //=============
        function Tool() {
        }

        Tool.prototype.isFunction = function (func) {
            return Type.isFunction(func);
        };
        Tool.prototype.isString = function (str) {
            return Type.isString(str);
        };
        Tool.prototype.isNumber = function (num) {
            return Type.isNumber(num);
        };
        Tool.prototype.isObject = function (obj) {
            return Type.isObject(obj);
        };
        Tool.prototype.isEmpty = function (o) {
            return Type.isUndefined(o) === true
                || Type.isNull(o) === true
                || o.length === 0
                || o === 'undefined';
        };
        iGeciku.tool = new Tool();

        //=============
        var iGecikuUrl = "",
            currentHost = window.location.host,
            currentPort = location.port;

        if (new RegExp("localhost|127.0.0.1").test(currentHost)) {
            iGecikuUrl = "//" + currentHost + "/";
        } else {
            iGecikuUrl = "//" + currentHost + "/";
        }

        function MyAjax() {
        }

        MyAjax.prototype.getByJson = function (url, data, callback, options) {
            url = iGecikuUrl + url;
            if (iGeciku.tool.isFunction(data) === false) {
                var str = '';
                for (var key in data) {
                    var temp = key + '=' + data[key];
                    str += temp + '&';
                }
                str = str.substr(0, str.length - 1);
                url += '?' + str;
            } else {
                var callback = data;
            }

            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'json',
                success: function (res) {
                    callback && callback(res);
                },
                error: function (err) {
                    callback && callback(err);
                }
            });
        };

        MyAjax.prototype.get = function (url, data, callback, options) {
            url = iGecikuUrl + url;
            if (iGeciku.tool.isFunction(data) === false) {
                var str = '';
                for (var key in data) {
                    var temp = key + '=' + data[key];
                    str += temp + '&';
                }
                str = str.substr(0, str.length - 1);
                url += '?' + str;
            } else {
                var callback = data;
            }

            $.ajax({
                type: 'GET',
                url: url,
                success: function (res) {
                    callback && callback(res);
                },
                error: function (err) {
                    callback && callback(err);
                }
            });
        };

        MyAjax.prototype.post = function (url, data, callback, options) {
            url = iGecikuUrl + url;
            var defaultOpts = {
                type: 'POST',
                url: url,
                data: data,
                dataType: 'json',
                success: function (res) {
                    if (res.code === -100) {
                        // TODO 登出
                    } else {
                        callback && callback(res);
                    }
                },
                error: function (err) {
                    callback && callback(err);
                }
            };

            var currentOpts = $.extend({}, defaultOpts, options);

            $.ajax(currentOpts);

        };

        iGeciku.ajax = new MyAjax();


        callback && callback(iGeciku);
    }

    // 加载函数
    win.iGeciku = iGeciku;

    iGeciku.onload_ = onload;

})(window);