P = new Page();
var map = new Map();

//分页对象
function Page() {
    this.config = [];
    this.version = '1.0';//分页版本

    //初始化参数
    this.initMathod = function (obj) {
        this.config = obj.params;
        this.renderPage();
    };

    //渲染分页
    this.renderPage = function () {
        for (var i = 0; i < P.config.length; i++) {
            map.put(P.config[i].elemId, P.config[i]);

            this.pageHtml(P.config[i]);

            //分页绑定事件
            $(P.config[i].elemId).on('click', 'a', function () {
                var flag = $(this).parent().hasClass('disabled');
                if (flag) {
                    return false;
                }

                var pageIndex = $(this).data('pageindex');
                var configObj = map.get($(this).data('elemid'));
                configObj.pageIndex = pageIndex;
                P.pageHtml(configObj);
            });
        }
    };

    //分页合成
    this.pageHtml = function (configObj) {
        var data = configObj;
        if (parseInt(data.total) <= 0) {
            return false;
        }

        var pageNum = isBlank(data.pageNum) ? 7 : parseInt(data.pageNum);//可显示页码个数
        var pageSize = isBlank(data.pageSize) ? 10 : parseInt(data.pageSize);//可显示页码个数
        var total = parseInt(data.total);//总记录数
        var pageTotal = total % pageSize != 0 ? parseInt(total / pageSize) + 1 : parseInt(total / pageSize);//总页数
        var pageIndex = pageTotal < parseInt(data.pageIndex) ? pageTotal : parseInt(data.pageIndex);//当前页
        var j = pageTotal < pageNum ? pageTotal : pageNum;//如果总页数小于可见页码，则显示页码为总页数
        var k = pageIndex < parseInt((j / 2) + 1) ? -1 * (pageIndex - 1) : pageIndex > (pageTotal - parseInt(j / 2)) ? -1 * (j - (pageTotal - pageIndex) - 1) : -1 * parseInt((j / 2));//遍历初始值
        var pageHtml = '<ul class="pagination pagination_type1 pagination_type3">';

        if (pageIndex <= 0 || pageIndex == 1) {
            pageHtml += '<li class="pagination__item disabled"><a class="pagination__number" data-elemid="' + configObj.elemId + '" data-pageindex="' + pageIndex + '"><</a></li>';
        } else {
            pageHtml += '<li class="pagination__item"><a class="pagination__number" data-elemid="' + configObj.elemId + '" onclick="javascript:toPage(this);" data-pageindex="' + (pageIndex - 1) + '"><</a></li>';
        }

        for (var i = k; i < (k + j); i++) {
            if (pageTotal == (pageIndex + i - 1)) break;
            if (i == 0) {
                pageHtml += '<li class="pagination__item"><span class="pagination__number pagination__number_active" data-pageindex="' + pageIndex + '">' + pageIndex + '</span></li>';
            } else {
                pageHtml += '<li class="pagination__item"><a class="pagination__number" data-elemid="' + configObj.elemId + '" onclick="javascript:toPage(this);" data-pageindex="' + (pageIndex + i) + '">' + (pageIndex + i) + '</a></li>';
            }
        }

        if (pageTotal == 1 || pageTotal <= pageIndex) {
            pageHtml += '<li class="pagination__item disabled"><a class="pagination__number" data-elemid="' + configObj.elemId + '" data-pageindex="' + pageTotal + '">></a></li>';
        } else {
            pageHtml += '<li class="pagination__item"><a class="pagination__number" data-elemid="' + configObj.elemId + '" onclick="javascript:toPage(this);" data-pageindex="' + (pageIndex + 1) + '">></a></li>';
        }
        pageHtml += '</ul>'
        $(configObj.elemId).html('');
        $(configObj.elemId).html(pageHtml);
    };
}

function isBlank(str) {
    if (str == undefined || str == null || str.trim() == '') {
        return true;
    }
    return false;
}

function Map() {
    this.elements = new Array();
    //获取MAP元素个数
    this.size = function () {
        return this.elements.length;
    };
    //判断MAP是否为空
    this.isEmpty = function () {
        return (this.elements.length < 1);
    };
    //删除MAP所有元素
    this.clear = function () {
        this.elements = new Array();
    };
    //向MAP中增加元素（key, value)
    this.put = function (_key, _value) {
        this.elements.push({
            key: _key,
            value: _value
        });
    };
    //删除指定KEY的元素，成功返回True，失败返回False
    this.remove = function (_key) {
        var bln = false;
        try {
            for (i = 0; i < this.elements.length; i++) {
                if (this.elements[i].key == _key) {
                    this.elements.splice(i, 1);
                    return true;
                }
            }
        } catch (e) {
            bln = false;
        }
        return bln;
    };
    //获取指定KEY的元素值VALUE，失败返回NULL
    this.get = function (_key) {
        try {
            for (i = 0; i < this.elements.length; i++) {
                if (this.elements[i].key == _key) {
                    return this.elements[i].value;
                }
            }
        } catch (e) {
            return null;
        }
    };
    //获取指定索引的元素（使用element.key，element.value获取KEY和VALUE），失败返回NULL
    this.element = function (_index) {
        if (_index < 0 || _index >= this.elements.length) {
            return null;
        }
        return this.elements[_index];
    };
    //判断MAP中是否含有指定KEY的元素
    this.containsKey = function (_key) {
        var bln = false;
        try {
            for (i = 0; i < this.elements.length; i++) {
                if (this.elements[i].key == _key) {
                    bln = true;
                }
            }
        } catch (e) {
            bln = false;
        }
        return bln;
    };
    //判断MAP中是否含有指定VALUE的元素
    this.containsValue = function (_value) {
        var bln = false;
        try {
            for (i = 0; i < this.elements.length; i++) {
                if (this.elements[i].value == _value) {
                    bln = true;
                }
            }
        } catch (e) {
            bln = false;
        }
        return bln;
    };
    //获取MAP中所有VALUE的数组（ARRAY）
    this.values = function () {
        var arr = new Array();
        for (i = 0; i < this.elements.length; i++) {
            arr.push(this.elements[i].value);
        }
        return arr;
    };
    //获取MAP中所有KEY的数组（ARRAY）
    this.keys = function () {
        var arr = new Array();
        for (i = 0; i < this.elements.length; i++) {
            arr.push(this.elements[i].key);
        }
        return arr;
    };
}
